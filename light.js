
var Light = function(id, color) {

    //get the target element to render the light into
    var elem = document.getElementById(id);


    //append a div into the target element
    var lightElem = document.createElement('div');
    lightElem.classList.add('light');
    lightElem.classList.add(color);
    elem.appendChild(lightElem);


    this.on = function(){
      lightElem.classList.add('on');
      // add the switch the light on logic here
    }

    this.off = function () {
      lightElem.classList.remove('on');
    }

    this.blink = function () {
      setInterval(function(){

      greenLight.on();
      setTimeout(function () {
        greenLight.off();
      },3000)

      orangeLight.on();
      setTimeout(function () {
        orangeLight.off();
      },3000)

      redLight.on();
      setTimeout(function () {
        redLight.off();
      },3000);

    },2000)
  }
}




// Instances

var redLight = new Light('light', 'red');
var orangeLight = new Light('light', 'orange');
var greenLight = new Light('light', 'green');


// greenLight.blink()
// orangeLight.blink()
// redLight.blink()





var blinkBtn = document.querySelector('.Bbtn');
var StopBtn = document.querySelector('.Stopbtn');
var GoBtn = document.querySelector('.Gobtn');
var Warnbtn=document.querySelector('.Warnbtn');

blinkBtn.addEventListener('click', function(){
    // let the light blink
console.log(blinkBtn);
    greenLight.blink();
    orangeLight.blink();
    redLight.blink();
});
GoBtn.addEventListener('click',function(){
  greenLight.on();
  redLight.off();
  orangeLight.off();
})
StopBtn.addEventListener('click', function(){
    // switch the light on/off

   redLight.on();
   greenLight.off();
   orangeLight.off();
});

Warnbtn.addEventListener('click', function(){
    // switch athe light on/off


orangeLight.on();
greenLight.off();
redLight.off();

});
